
#Install lastest version
sudo add-apt-repository ppa:webupd8team/sublime-text-3
sudo apt-get update
sudo apt-get install sublime-text-installer

#Install my packages
PACKAGE_DIR="/home/$USER/.config/sublime-text-3/Packages"
mkdir $PACKAGE_DIR
cd $PACKAGE_DIR

mkdir "Agila Theme"
curl -L https://github.com/arvi/Agila-Theme/tarball/master | sudo tar zx -C "Agila Theme" --strip-components=1
mkdir "TerminalView"
curl -L https://github.com/Wramberg/TerminalView/tarball/master | sudo tar zx -C "TerminalView" --strip-components=1
mkdir "FileFinder"
curl -L https://github.com/houcheng/Filefinder/tarball/master | sudo tar zx -C "FileFinder" --strip-components=1
mkdir "Move by symbol"
curl -L https://github.com/abusalimov/SublimeMoveBySymbols/tarball/master | sudo tar zx -C "Move by symbol" --strip-components=1
mkdir "Origami"
curl -L https://github.com/SublimeText/Origami/tarball/master | sudo tar zx -C "Origami" --strip-components=1
mkdir "Pane Pane"
curl -L https://github.com/mjsmith1028/PanePane/tarball/master | sudo tar zx -C "Pane Pane" --strip-components=1
mkdir "Text-Pastry"
curl -L https://github.com/duydao/Text-Pastry/tarball/master | sudo tar zx -C "Text-Pastry" --strip-components=1

#Configure settings
SETTINGS_DIR="/home/$USER/.config/sublime-text-3/Packages/User"
cd $SETTINGS_DIR
wget https://bit.ly/2mb3tNW -O "Default (Linux).sublime-keymap"
wget https://bit.ly/2JeiGGQ -O "Preferences.sublime-settings"
